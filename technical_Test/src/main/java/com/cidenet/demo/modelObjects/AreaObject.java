package com.cidenet.demo.modelObjects;

public class AreaObject {
    private int idArea;
    private String nameArea;

    public AreaObject(int idArea, String nameArea) {
        this.idArea = idArea;
        this.nameArea = nameArea;
    }

    public AreaObject() {
    }

    public int getIdArea() {
        return idArea;
    }

    public void setIdArea(int idArea) {
        this.idArea = idArea;
    }

    public String getNameArea() {
        return nameArea;
    }

    public void setNameArea(String nameArea) {
        this.nameArea = nameArea;
    }
}
