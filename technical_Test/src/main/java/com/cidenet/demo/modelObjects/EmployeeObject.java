package com.cidenet.demo.modelObjects;

import java.util.Date;

/**
 * @author Danilo Zuñiga
 * @version 1.0.0
 * @date 02/03/2020
 */

public class EmployeeObject {
    private String firstSurname, secondSurname, firstName,
            othersName, identification, typeIdentification, area, dateRegister, email, country,stateEmployee;
    private int idTypeIdentification, idArea, idCountry;
    private Date dateEntry;

    public EmployeeObject(String firstSurname, String secondSurname, String firstName, String othersName,
                          String identification, Date dateEntry, String email, int idTypeIdentification,
                          int idArea, int idCountry) {
        this.firstSurname = firstSurname;
        this.secondSurname = secondSurname;
        this.firstName = firstName;
        this.othersName = othersName;
        this.identification = identification;
        this.dateEntry = dateEntry;
        this.email = email;
        this.idTypeIdentification = idTypeIdentification;
        this.idArea = idArea;
        this.idCountry = idCountry;
    }

    public EmployeeObject() {
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public int getIdTypeIdentification() {
        return idTypeIdentification;
    }

    public void setIdTypeIdentification(int idTypeIdentification) {
        this.idTypeIdentification = idTypeIdentification;
    }

    public int getIdArea() {
        return idArea;
    }

    public void setIdArea(int idArea) {
        this.idArea = idArea;
    }

    public int getIdCountry() {
        return idCountry;
    }

    public void setIdCountry(int idCountry) {
        this.idCountry = idCountry;
    }

    public String getFirstSurname() {
        return firstSurname;
    }

    public void setFirstSurname(String firstSurname) {
        this.firstSurname = firstSurname;
    }

    public String getSecondSurname() {
        return secondSurname;
    }

    public void setSecondSurname(String secondSurname) {
        this.secondSurname = secondSurname;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getOthersName() {
        return othersName;
    }

    public void setOthersName(String otherName) {
        this.othersName = otherName;
    }

    public String getIdentification() {
        return identification;
    }

    public void setIdentification(String identification) {
        this.identification = identification;
    }

    public String getTypeIdentification() {
        return typeIdentification;
    }

    public void setTypeIdentification(String typeIdentification) {
        this.typeIdentification = typeIdentification;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public Date getDateEntry() {
        return dateEntry;
    }

    public void setDateEntry(Date dateEntry) {
        this.dateEntry = dateEntry;
    }

    public String getDateRegister() {
        return dateRegister;
    }

    public void setDateRegister(String dateRegister) {
        this.dateRegister = dateRegister;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getStateEmployee() {
        return stateEmployee;
    }

    public void setStateEmployee(String stateEmployee) {
        this.stateEmployee = stateEmployee;
    }

    @Override
    public String toString() {
        return "EmployeeObject{" +
                "firstSurname='" + firstSurname + '\'' +
                ", secondSurname='" + secondSurname + '\'' +
                ", firstName='" + firstName + '\'' +
                ", othersName='" + othersName + '\'' +
                ", identification='" + identification + '\'' +
                ", typeIdentification='" + typeIdentification + '\'' +
                ", area='" + area + '\'' +
                ", dateRegister='" + dateRegister + '\'' +
                ", email='" + email + '\'' +
                ", country='" + country + '\'' +
                ", stateEmployee='" + stateEmployee + '\'' +
                ", idTypeIdentification=" + idTypeIdentification +
                ", idArea=" + idArea +
                ", idCountry=" + idCountry +
                ", dateEntry=" + dateEntry +
                '}';
    }
}
