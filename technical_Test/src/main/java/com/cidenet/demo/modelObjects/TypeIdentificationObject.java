package com.cidenet.demo.modelObjects;

public class TypeIdentificationObject {
   private int idTypeIdentification;
   private String nameIdentification;

    public TypeIdentificationObject(int idTypeIdentification, String nameIdentification) {
        this.idTypeIdentification = idTypeIdentification;
        this.nameIdentification = nameIdentification;
    }

    public TypeIdentificationObject() {
    }

    public int getIdTypeIdentification() {
        return idTypeIdentification;
    }

    public void setIdTypeIdentification(int idTypeIdentification) {
        this.idTypeIdentification = idTypeIdentification;
    }

    public String getNameIdentification() {
        return nameIdentification;
    }

    public void setNameIdentification(String nameIdentification) {
        this.nameIdentification = nameIdentification;
    }
}
