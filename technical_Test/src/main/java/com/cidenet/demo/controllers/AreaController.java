package com.cidenet.demo.controllers;

import com.cidenet.demo.interfaces.IAreaModel;
import com.cidenet.demo.interfaces.ICountryModel;
import com.cidenet.demo.modelObjects.AreaObject;
import com.cidenet.demo.modelObjects.CountryObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author Danilo Zúñiga
 * @version 1.0.0
 * @date 03/03/2020
 */
@Component
public class AreaController {

    private IAreaModel model;

    @Autowired
    AreaController(final IAreaModel model) {
        this.model = model;
    }

    /**
     * Get the list of all the area
     *
     * @return the list of the areaObject
     */
    @Autowired
    public List<AreaObject> areaObjectList() {
        return model.areaObjectList();
    }
}
