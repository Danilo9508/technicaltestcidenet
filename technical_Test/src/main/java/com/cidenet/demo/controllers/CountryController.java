package com.cidenet.demo.controllers;

import com.cidenet.demo.interfaces.ICountryModel;
import com.cidenet.demo.modelObjects.CountryObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author Danilo Zúñiga
 * @version 1.0.0
 * @date 01/03/2020
 */
@Component
public class CountryController {

    private ICountryModel country;

    @Autowired
    CountryController(final ICountryModel country) {
        this.country = country;
    }

    /**
     * Get the list of all the countries
     *
     * @return the list of the countries
     */

    public List<CountryObject> countryObjectList() {
        return country.countryObjectList();
    }
}
