package com.cidenet.demo.controllers;

import com.cidenet.demo.interfaces.ICountryModel;
import com.cidenet.demo.interfaces.ITypeIdentificationModel;
import com.cidenet.demo.modelObjects.CountryObject;
import com.cidenet.demo.modelObjects.TypeIdentificationObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author Danilo Zúñiga
 * @version 1.0.0
 * @date 01/03/2020
 */

@Component
public class TypeIdentificationController {
    private ITypeIdentificationModel typeIdentificationModel;

    @Autowired
    TypeIdentificationController(final ITypeIdentificationModel country) {
        this.typeIdentificationModel = country;
    }

    /**
     * Get the list of all the typeIdentification
     *
     * @return the list of the typeIdentificationObject
     */
    public List<TypeIdentificationObject> typeIdentificationModelList() {
        return typeIdentificationModel.typeIdentificationModelList();
    }
}
