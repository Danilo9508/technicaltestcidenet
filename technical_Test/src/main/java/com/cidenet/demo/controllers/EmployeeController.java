package com.cidenet.demo.controllers;

import com.cidenet.demo.interfaces.IEmployeeModel;
import com.cidenet.demo.modelObjects.EmployeeObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author Danilo Zuñiga
 * @version 1.0.0
 * @date 03/03/2020
 */
@Component
public class EmployeeController {
    private IEmployeeModel iEmployeeModel;

    @Autowired
    EmployeeController(final IEmployeeModel iEmployeeModel) {
        this.iEmployeeModel = iEmployeeModel;
    }

    /**
     * Validate if the employee's email exists
     *
     * @return Integer
     */

    public int validateEmployeeEmail(String mail) {
        return iEmployeeModel.validateEmployeeEmail(mail);
    }
    /**
     * Validate if the employee's email exists
     *
     * @return Integer
     */
    public int validateEmployeeEmailUpdate(String mail,String cc) {
        return iEmployeeModel.validateEmployeeEmailUpdate(mail, cc);
    }

    /**
     * Insert employee data
     *
     * @return boolean
     */
    public int insertEmployee(EmployeeObject employeeObject) {

        return iEmployeeModel.insertEmployee(employeeObject);
    }

    /**
     * Update employee data
     *
     * @return boolean
     */
    public int updateEmployee(EmployeeObject employeeObject, String cc) {

        return iEmployeeModel.updateEmployee(employeeObject,cc);
    }

    /**
     * Get the list of all the employees
     *
     * @return the list of the employeeObject
     */
    @Autowired
    public List<EmployeeObject> employeeObjectList() {

        return iEmployeeModel.employeeObjectList();
    }

    /**
     * Get the Object the employees
     *
     * @return the Object the employeeObject
     */

    public EmployeeObject employeeObjectID(String identification) {
        return iEmployeeModel.employeeObjectID(identification);
    }

    /**
     * Delete employee
     *
     * @return 1 if delete or 0 if not delete
     */
    public int deleteEmployee(String ident){
        return iEmployeeModel.deleteEmployee(ident);


    }
}
