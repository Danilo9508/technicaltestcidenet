package com.cidenet.demo.models;

import com.cidenet.demo.interfaces.ITypeIdentificationModel;
import com.cidenet.demo.modelObjects.TypeIdentificationObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Danilo Zúñiga
 * @version 1.0.0
 * @date 01/03/2020
 */
@Repository
public class TypeIdentificationModel implements ITypeIdentificationModel {

    private JdbcTemplate template;


    @Autowired
    public TypeIdentificationModel(final JdbcTemplate template) {
        this.template = template;
    }

    /**
     * Get the list of all the typeIdentification
     *
     * @return the list of the typeIdentificationObject
     */

    @Override
    public List<TypeIdentificationObject> typeIdentificationModelList() {

        try {
            String sql = "select * from typeIdentification";
            return template.query(sql, new BeanPropertyRowMapper<TypeIdentificationObject>(TypeIdentificationObject.class));

        } catch (Exception e) {
            System.out.println("type "+e.getMessage());
        }
        return null;
    }
}
