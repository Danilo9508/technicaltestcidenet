package com.cidenet.demo.models;


import com.cidenet.demo.interfaces.IEmployeeModel;
import com.cidenet.demo.modelObjects.CountryObject;
import com.cidenet.demo.modelObjects.EmployeeObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.validation.BeanPropertyBindingResult;

import java.util.List;

/**
 * @author Danilo Zuñiga
 * @version 1.0.0
 * @date 03/03/2020
 */
@Repository
public class EmployeeModel implements IEmployeeModel {
    private JdbcTemplate template;

    @Autowired
    EmployeeModel(final JdbcTemplate template) {
        this.template = template;
    }

    /**
     * Validate if the employee's email exists
     *
     * @return Integer
     */

    @Override
    public int validateEmployeeEmail(String mail) {
        int response = 0;
        try {
            String sql = "select COUNT(*) from employees where email='" + mail+"'";
            response = template.queryForObject(sql, Integer.class);
        } catch (Exception e) {
            System.out.println("validate email " + e.getMessage());

        }
        return response;
    }

    /**
     * Validate if the employee's email exists
     *
     * @return Integer
     */

    @Override
    public int validateEmployeeEmailUpdate(String mail, String cc) {
        int response = 0;
        try {
            String sql = "select COUNT(*) from employees where email='" + mail + "' and identification!='" + cc+"'";
            response = template.queryForObject(sql, Integer.class);
        } catch (Exception e) {
            System.out.println("validate email " + e.getMessage());

        }
        return response;
    }

    /**
     * Get the list of all the employee
     *
     * @return the list of the employee
     */

    @Override
    public List<EmployeeObject> employeeObjectList() {
        try {
            String sql = "select * from employees";
            return template.query(sql, new BeanPropertyRowMapper<EmployeeObject>(EmployeeObject.class));
        } catch (Exception e) {
            System.out.println("employees " + e.getMessage());

        }
        return null;

    }

    /**
     * Insert employee
     *
     * @return Integer 0 if insert ok or 1 if insert error
     */

    @Override
    public int insertEmployee(EmployeeObject employeeObject) {
        System.out.println(employeeObject.toString());
        int response = 0;
        try {
            String sql = "insert into employee(firstSurname,secondSurname,firstName,othersName," +
                    "identification,email,dateEntry,idAreaFk,typeIdentificationFk,idCountryFk) values(?,?,?,?,?,?,?,?,?,?)";
            response = template.update(sql,
                    employeeObject.getFirstSurname(),
                    employeeObject.getSecondSurname(),
                    employeeObject.getFirstName(),
                    employeeObject.getOthersName(),
                    employeeObject.getIdentification(),
                    employeeObject.getEmail(),
                    employeeObject.getDateEntry(),
                    employeeObject.getIdArea(),
                    employeeObject.getIdTypeIdentification(),
                    employeeObject.getIdCountry()
            );
            return response;
        } catch (Exception e) {
            System.out.println("insert employee " + e);
        }
        return response;
    }

    /**
     * Update employee
     *
     * @return Integer 0 if insert ok or 1 if insert error
     */

    @Override
    public int updateEmployee(EmployeeObject employeeObject, String cc) {

        int response = 0;
        try {
            String sql = "update employee set firstSurname=?,secondSurname=?,firstName=?,othersName=?," +
                    "identification=?,email=?,dateEntry=?,idAreaFk=?,typeIdentificationFk=?,idCountryFk=? " +
                    "where identification=" + cc;
            response = template.update(sql,
                    employeeObject.getFirstSurname(),
                    employeeObject.getSecondSurname(),
                    employeeObject.getFirstName(),
                    employeeObject.getOthersName(),
                    employeeObject.getIdentification(),
                    employeeObject.getEmail(),
                    employeeObject.getDateEntry(),
                    employeeObject.getIdArea(),
                    employeeObject.getIdTypeIdentification(),
                    employeeObject.getIdCountry()
            );
            return response;
        } catch (Exception e) {
            System.out.println("insert employee " + e);
        }
        return response;
    }

    /**
     * Get Object the employee
     *
     * @return the Object of the employee
     */

    @Override
    public EmployeeObject employeeObjectID(String identification) {
        try {
            String sql = "select * from employee where identification=?";
            return template.queryForObject(sql, new Object[]{identification}, new BeanPropertyRowMapper<EmployeeObject>(EmployeeObject.class));
        } catch (Exception e) {
            System.out.println("employees id " + e.getMessage());
        }
        return null;
    }
    /**
     * Get Object the employee
     *
     * @return the Object of the employee
     */
    @Override
    public int deleteEmployee(String ident) {
        int response = 0;
        try {
            String sql = "update employee set stateEmployee='INACTIVO' where identification=" + ident;
            response = template.update(sql);
        } catch (Exception e) {
            System.out.println("employees delete " + e.getMessage());
        }
        return response;
    }


}
