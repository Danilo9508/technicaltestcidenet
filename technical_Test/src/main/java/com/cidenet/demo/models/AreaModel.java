package com.cidenet.demo.models;

import com.cidenet.demo.interfaces.IAreaModel;
import com.cidenet.demo.modelObjects.AreaObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Danilo Zúñiga
 * @version 1.0.0
 * @date 03/03/2020
 */
@Repository
public class AreaModel implements IAreaModel {
    private JdbcTemplate template;

    @Autowired
    public AreaModel(final JdbcTemplate template) {
        this.template = template;
    }

    /**
     * Get the list of all areas from the database
     *
     * @return the list of the areas
     */
    @Override
    public List<AreaObject> areaObjectList() {
        try {
            String sql = "select * from area";
            return template.query(sql, new BeanPropertyRowMapper<AreaObject>(AreaObject.class));
        } catch (Exception e) {
            System.out.println("area "+e.getMessage());
        }
        return null;
    }
}
