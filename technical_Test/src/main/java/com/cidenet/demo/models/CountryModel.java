package com.cidenet.demo.models;

import com.cidenet.demo.interfaces.ICountryModel;
import com.cidenet.demo.modelObjects.CountryObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CountryModel implements ICountryModel {

    private JdbcTemplate template;

    @Autowired
    public CountryModel(final JdbcTemplate template) {
        this.template = template;
    }

    /**
     * Get the list of all countries from the database
     *
     * @return the list of the countries
     */
    @Override
    public List<CountryObject> countryObjectList() {
        try {
            String sql = "select * from country";
            return template.query(sql, new BeanPropertyRowMapper<CountryObject>(CountryObject.class));
        } catch (Exception e) {
            System.out.println("countries "+e.getMessage());
        }
        return null;
    }
}
