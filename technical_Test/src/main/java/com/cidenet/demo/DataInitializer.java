package com.cidenet.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * @author Danilo Zúñiga
 * @version 1.0.0
 * @date 03/03/2020
 */
@Component
@Order(1)
public class DataInitializer {

    private static JdbcTemplate template;

    @Autowired
    public DataInitializer(final JdbcTemplate template) {
        this.template = template;
    }

    /**
     * Loads the database structure into memory
     *
     * @return none
     */
    @PostConstruct
    public void loadH2DataBase() {

        /**
         *
         * crate table area
         * */

        template.execute("DROP TABLE IF EXISTS area");
        template.execute("CREATE TABLE  area (" +
                "  idArea INT NOT NULL PRIMARY KEY AUTO_INCREMENT," +
                "  nameArea VARCHAR(100) NOT NULL," +
                "  stateArea VARCHAR(1) NOT NULL DEFAULT 'I')");

        template.update("INSERT INTO area(nameArea) values('ADMINISTRACIÓN')");
        template.update("INSERT INTO area(nameArea) values('FINANCIERA')");
        template.update("INSERT INTO area(nameArea) values('COMPRAS')");
        template.update("INSERT INTO area(nameArea) values('OPERACIÓN')");
        template.update("INSERT INTO area(nameArea) values('TALENTO HUMANO')");
        template.update("INSERT INTO area(nameArea) values('SERVICIOS VARIOS')");

        /**
         *
         * crate table typeIdentification
         * */

        template.execute("DROP TABLE IF EXISTS typeIdentification");
        template.execute("CREATE TABLE typeIdentification (" +
                "  idTypeIdentification INT NOT NULL PRIMARY KEY AUTO_INCREMENT," +
                "  nameIdentification VARCHAR(80) NOT NULL," +
                "  state VARCHAR(1) NOT NULL DEFAULT 'A')");

        template.update("INSERT INTO typeIdentification(nameIdentification) values('CÉDULA DE CIUDADANIA')");
        template.update("INSERT INTO typeIdentification(nameIdentification) values('CÉDULA DE EXTRANGERÍA')");
        template.update("INSERT INTO typeIdentification(nameIdentification) values('PASAPORTE')");
        template.update("INSERT INTO typeIdentification(nameIdentification) values('PERMISO ESPECIAL')");

        /**
         *
         * crate table country
         * */

        template.execute("DROP TABLE IF EXISTS country");
        template.execute("CREATE TABLE  country (" +
                "  idCountry INT NOT NULL PRIMARY KEY AUTO_INCREMENT," +
                "  nameCountry VARCHAR(45) NOT NULL," +
                "  stateCountry VARCHAR(1) NOT NULL DEFAULT 'A')");

        template.update("INSERT INTO country(nameCountry) values('COLOMBIA')");
        template.update("INSERT INTO country(nameCountry) values('ESTADOS UNIDOS')");

        /**
         *
         * crate table employee
         * */

        // template.execute("DROP TABLE IF EXISTS employee");

        template.execute("CREATE TABLE IF NOT EXISTS employee (" +
                "  idEmployee INT NOT NULL AUTO_INCREMENT," +
                "  firstSurname VARCHAR(20) NOT NULL," +
                "  secondSurname VARCHAR(20) NOT NULL," +
                "  firstName VARCHAR(20) NOT NULL," +
                "  othersName VARCHAR(50)," +
                "  identification VARCHAR(20) NOT NULL," +
                "  email VARCHAR(300) NOT NULL," +
                "  dateEntry DATE NOT NULL ," +
                "  stateEmployee VARCHAR(10) NOT NULL DEFAULT 'ACTIVO'," +
                "  dateRegister DATETIME NOT NULL DEFAULT now()," +
                "  idAreaFk INT NOT NULL," +
                "  typeIdentificationFk INT NOT NULL," +
                "  idCountryFk INT NOT NULL," +
                "  dateUpdate DATETIME ," +
                "  PRIMARY KEY (idEmployee, idAreaFk, typeIdentificationFk, idcountryFk)," +
                "  UNIQUE INDEX identification_UNIQUE (identification)," +
                "  UNIQUE INDEX email_UNIQUE (email))");

        template.execute("ALTER TABLE employee ADD FOREIGN KEY (idAreaFk) REFERENCES area(idArea) ");
        template.execute("ALTER TABLE employee ADD FOREIGN KEY (typeIdentificationFk)" +
                " REFERENCES typeIdentification(idTypeIdentification) ");

        template.execute("ALTER TABLE employee ADD FOREIGN KEY (idCountryFk) REFERENCES country(idCountry) ");

        /*
        template.update("INSERT INTO employee(firstSurname,secondSurname,firstName,identification,email,dateEntry,idAreaFk" +
                ",typeIdentificationFk,idCountryFk) values('PRUEBA', 'PRUEBA2','PRUEBA3','123443','PRUEBA3.PRUEBA@CIDENET.COM.CO'," +
                "'2021-03-03',1,1,1)");
         */


        /**
         *
         * crate view employees
         * */
        template.execute("create or replace view  employees as (select e.*,t.nameIdentification as typeIdentification," +
                " c.nameCountry as country, a.nameArea as area from employee e inner join typeIdentification" +
                " t on e.typeIdentificationFk=t.idTypeIdentification inner join country c on e.idCountryFk=c.idCountry" +
                " inner join area a on e.idAreaFk=a.idArea where stateEmployee='ACTIVO')");

    }
}
