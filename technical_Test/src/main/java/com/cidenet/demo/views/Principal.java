package com.cidenet.demo.views;

import com.cidenet.demo.complements.Utilities;
import com.cidenet.demo.complements.Validation;
import com.cidenet.demo.controllers.AreaController;
import com.cidenet.demo.controllers.CountryController;
import com.cidenet.demo.controllers.EmployeeController;
import com.cidenet.demo.controllers.TypeIdentificationController;
import com.cidenet.demo.modelObjects.AreaObject;
import com.cidenet.demo.modelObjects.CountryObject;
import com.cidenet.demo.modelObjects.EmployeeObject;
import com.cidenet.demo.modelObjects.TypeIdentificationObject;
import com.cidenet.demo.tableDesing.MyScrollbarUI;
import com.cidenet.demo.tableDesing.Render;
import com.cidenet.demo.tableDesing.StyleTableHeader;
import com.cidenet.demo.tableDesing.StyleTableRenderer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import rojeru_san.componentes.RSDateChooser;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.*;
import java.util.Date;
import java.util.List;

/**
 * @author Danilo Zúñiga
 * @version 1.0.0
 * @date 01/03/2020
 */

@Component
public class Principal extends JFrame {
    private JPanel panel1;
    private JPanel panelTop;
    private JPanel panelLeft;
    private JPanel panelCenter;
    private JLabel label_form;
    private JButton btnListUser;
    private JButton btnAddUser;
    private JPanel panelList;
    private JPanel panelForm;
    private JPanel panelTitle;
    private JPanel panelTitle2;
    private JPanel panelFilterTable;
    private JPanel panelFilter;
    private JComboBox filter;
    private JTextField textSearch;
    private JButton btnSearch;
    private JTable tabledata;
    private JScrollPane scroll;
    private JPanel panelOptions;
    private JButton btnEdit;
    private JButton btnDelete;
    private JPanel panelContainer;
    private JPanel panelDataForm;
    private JTextField textFirtsSurname;
    private JTextField textSecondSurname;
    private JTextField textFirstName;
    private JComboBox typeIdentification;
    private JRadioButton radioColombia;
    private JRadioButton radioEU;
    private JTextField textIdentification;
    private JComboBox area;
    private JTextField textOtherName;
    private JPanel panelCountry;
    private JButton btnRegister;
    private RSDateChooser admissionDate;
    private JLabel titleTable;
    private JLabel titleForm;
    private CardLayout card;
    private boolean btn_list = true, btnUser = false;
    private ButtonGroup group;
    private JRadioButton[] countriesOption;
    private List<EmployeeObject> listAux;
    private String ident;

    //controllers
    private CountryController countryController;
    private TypeIdentificationController typeIdentificationController;
    private AreaController areaController;
    private EmployeeController employeeController;
    private Validation validation;
    private Utilities utilities;

    @Autowired
    public Principal(final CountryController countryController,
                     final TypeIdentificationController typeIdentificationController,
                     final AreaController areaController,
                     final EmployeeController employeeController,
                     final Validation validation,
                     final Utilities utilities) {
        this.countryController = countryController;
        this.typeIdentificationController = typeIdentificationController;
        this.areaController = areaController;
        this.employeeController = employeeController;
        this.validation = validation;
        this.utilities = utilities;

        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setContentPane(panel1);
        setMinimumSize(new Dimension(800, 650));
        setExtendedState(MAXIMIZED_BOTH);
        setLocationRelativeTo(this);
        initComponents();
        loadAllData();
        cleanForm();
        setVisible(true);


    }

    /**
     * load all data from the database
     *
     * @return none
     */
    private void loadAllData() {
        loadCountries();
        loadEmployees();
        loadTypeIdentification();
        loadArea();
    }

    /**
     * Loading events, functions, and form components
     *
     * @return none
     */

    private void initComponents() {
        ImageIcon image1 = new ImageIcon("src/Icons/group.png");
        btnListUser.setIcon(image1);
        btnListUser.setCursor(new Cursor(Cursor.HAND_CURSOR));
        btnListUser.setBackground(new Color(158, 158, 158));

        ImageIcon image2 = new ImageIcon("src/Icons/adduser.png");
        btnAddUser.setIcon(image2);
        btnAddUser.setCursor(new Cursor(Cursor.HAND_CURSOR));

        ImageIcon image3 = new ImageIcon("src/Icons/edit.png");
        btnEdit.setIcon(image3);
        btnEdit.setCursor(new Cursor(Cursor.HAND_CURSOR));

        ImageIcon image4 = new ImageIcon("src/Icons/delete.png");
        btnDelete.setIcon(image4);
        btnDelete.setCursor(new Cursor(Cursor.HAND_CURSOR));

        card = (CardLayout) panelCenter.getLayout();


        radioColombia.setVisible(false);
        radioEU.setVisible(false);

        //filter data
        String[] dataFilter = {
                "Primer Nombre", "Otros Nombres", "Primer Apellido", "TP. Identificación", "# Identificación",
                "País del empleo", "Correo", "Estado"
        };


        filter.setModel(new DefaultComboBoxModel(dataFilter));
        filter.setBorder(BorderFactory.createTitledBorder("Filtrar por : "));

        //input search
        textSearch.setBorder(BorderFactory.createTitledBorder("Buscar: "));
        btnSearch.setCursor(new Cursor(Cursor.HAND_CURSOR));

        //desing form
        panelCountry.setBorder(BorderFactory.createTitledBorder("Pais: "));
        textFirstName.setBorder(BorderFactory.createTitledBorder("Primer Nombre: "));
        textFirtsSurname.setBorder(BorderFactory.createTitledBorder("Primer Apellido : "));
        textSecondSurname.setBorder(BorderFactory.createTitledBorder("Segundo Apellido: "));
        textOtherName.setBorder(BorderFactory.createTitledBorder("Otros Nombres: "));
        textIdentification.setBorder(BorderFactory.createTitledBorder("Número de Identificación : "));
        area.setBorder(BorderFactory.createTitledBorder("Área : "));
        typeIdentification.setBorder(BorderFactory.createTitledBorder("Tipo de Identificación : "));
        admissionDate.setBorder(BorderFactory.createTitledBorder("Fecha de Ingreso : "));
        btnRegister.setCursor(new Cursor(Cursor.HAND_CURSOR));

        // dateChooser
        admissionDate.setFormatoFecha("dd/MM/yyyy");
        admissionDate.setDatoFecha(new Date());


        //table layout in view

        tableDesing();
        alingTable();
        columnSize();


        /**
         *
         *events components
         *
         */

        btnDelete.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (tabledata.getSelectedRow() >= 0) {
                    int option = JOptionPane.showConfirmDialog(panel1, "¿Está seguro de que desea eliminar el empleado?",
                            "", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.WARNING_MESSAGE);
                    if (option == 0) {
                        deleteEmployee(tabledata.getSelectedRow());
                        loadEmployees();
                    }
                }

            }
        });

        btnEdit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                sendDataForm(tabledata.getSelectedRow());
            }
        });

        textSearch.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent keyEvent) {
                if (Character.isLowerCase(keyEvent.getKeyChar())) {
                    String msj = ("" + keyEvent.getKeyChar()).toUpperCase();
                    keyEvent.setKeyChar(msj.charAt(0));
                }
            }
        });

        btnSearch.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {

                filterDataEmployee(filter.getSelectedIndex(), textSearch.getText());

            }
        });

        textSearch.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent keyEvent) {
                filterDataEmployee(filter.getSelectedIndex(), textSearch.getText());
            }
        });

        tabledata.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent mouseEvent) {

                validation.validateButtons(btnEdit, btnDelete, tabledata.getSelectedRow());
            }
        });
        btnRegister.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (btnRegister.getText().equals("Registrar")) {
                    insertEmployee();
                    btnEdit.setEnabled(false);
                    btnDelete.setEnabled(false);
                } else {

                    updateEmployee();
                    btnEdit.setEnabled(false);
                    btnDelete.setEnabled(false);
                    card.show(panelCenter, "CardList");
                    loadEmployees();
                }
            }
        });

        btnListUser.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseEntered(MouseEvent mouseEvent) {
                btnListUser.setBackground(new Color(178, 178, 178));
            }

            @Override
            public void mouseExited(MouseEvent mouseEvent) {
                if (!btn_list) {
                    btnListUser.setBackground(new Color(187, 187, 187));
                } else {
                    btnListUser.setBackground(new Color(158, 158, 158));
                }
            }
        });

        btnAddUser.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseEntered(MouseEvent mouseEvent) {
                btnAddUser.setBackground(new Color(178, 178, 178));
            }

            @Override
            public void mouseExited(MouseEvent mouseEvent) {
                if (!btnUser) {
                    btnAddUser.setBackground(new Color(187, 187, 187));
                } else {
                    btnAddUser.setBackground(new Color(158, 158, 158));
                }

            }

        });

        btnListUser.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                btnListUser.setBackground(new Color(158, 158, 158));
                btnAddUser.setBackground(new Color(187, 187, 187));
                btn_list = true;
                btnUser = false;
                btnEdit.setEnabled(false);
                btnDelete.setEnabled(false);
                loadEmployees();
                card.show(panelCenter, "CardList");
            }
        });

        btnAddUser.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                btnAddUser.setBackground(new Color(158, 158, 158));
                btnListUser.setBackground(new Color(187, 187, 187));
                btnUser = true;
                btn_list = false;
                titleForm.setText("Nuevo Empleado");
                btnRegister.setText("Registrar");
                btnEdit.setEnabled(false);
                btnDelete.setEnabled(false);
                cleanForm();
                card.show(panelCenter, "CardForm");
            }
        });

        KeyAdapter listener = new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent keyEvent) {
                if (keyEvent.getSource() == textFirtsSurname) {
                    validation.validateOnlyLetters(keyEvent);
                    if (textFirtsSurname.getText().length() > 19) {
                        keyEvent.consume();
                    }
                }
                if (keyEvent.getSource() == textSecondSurname) {
                    validation.validateOnlyLetters(keyEvent);
                    if (textSecondSurname.getText().length() > 19) {
                        keyEvent.consume();
                    }
                }
                if (keyEvent.getSource() == textFirstName) {
                    validation.validateOnlyLetters(keyEvent);
                    if (textFirstName.getText().length() > 19) {
                        keyEvent.consume();
                    }
                }
                if (keyEvent.getSource() == textOtherName) {
                    validation.validateOnlyLetters(keyEvent);
                    if (textOtherName.getText().length() > 19) {
                        keyEvent.consume();
                    }
                }
                if (keyEvent.getSource() == textIdentification) {
                    validation.validateOnlyNumberLetters(keyEvent);
                    if (textIdentification.getText().length() > 19) {
                        keyEvent.consume();
                    }
                }
            }
        };

        textFirtsSurname.addKeyListener(listener);
        textSecondSurname.addKeyListener(listener);
        textFirstName.addKeyListener(listener);
        textOtherName.addKeyListener(listener);
        textIdentification.addKeyListener(listener);
    }

    /**
     * Improve table layout in view
     *
     * @return none
     */

    private void tableDesing() {
        tabledata.setModel(new DefaultTableModel(
                new Object[][]{

                },
                new String[]{
                        "Primer Nombre", "Otros Nombres", "Primer Apellido", "TP. Identificación", "# Identificación",
                        "País del empleo", "Correo", "Estado"
                }
        ) {
            boolean[] canEdit = new boolean[]{
                    false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit[columnIndex];
            }
        });
        tabledata.setCursor(new Cursor(Cursor.HAND_CURSOR));
        tabledata.setRowHeight(30);
        tabledata.setSelectionBackground(new Color(92, 125, 152));
        tabledata.setSelectionForeground(new Color(255, 255, 255));
        tabledata.setShowHorizontalLines(false);
        tabledata.setShowVerticalLines(false);
        tabledata.getTableHeader().setDefaultRenderer(new StyleTableHeader(52, 73, 94));
        tabledata.setDefaultRenderer(Object.class, new StyleTableRenderer(81, 86, 209, 54, 59, 203));
        tabledata.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        scroll.getViewport().setBackground(new Color(255, 255, 255));
        scroll.getVerticalScrollBar().setUI(new MyScrollbarUI(54, 59, 203));
        scroll.getHorizontalScrollBar().setUI(new MyScrollbarUI(54, 59, 203));
    }

    /**
     * Center the employee table data
     *
     * @return none
     */

    private void alingTable() {
        DefaultTableCellRenderer Alinear = new DefaultTableCellRenderer();
        Alinear.setHorizontalAlignment(SwingConstants.CENTER);
        for (int i = 0; i < 8; i++) {
            tabledata.getColumnModel().getColumn(i).setCellRenderer(Alinear);
        }

    }

    /**
     * Change the size of the columns
     *
     * @return none
     */
    private void columnSize() {
        tabledata.getColumnModel().getColumn(0).setPreferredWidth(80);
        tabledata.getColumnModel().getColumn(1).setPreferredWidth(80);
        tabledata.getColumnModel().getColumn(2).setPreferredWidth(80);
        tabledata.getColumnModel().getColumn(3).setPreferredWidth(80);
        tabledata.getColumnModel().getColumn(4).setPreferredWidth(80);
        tabledata.getColumnModel().getColumn(5).setPreferredWidth(80);
        tabledata.getColumnModel().getColumn(6).setPreferredWidth(300);
        tabledata.getColumnModel().getColumn(7).setPreferredWidth(10);

    }

    /**
     * Get and show all employees in view
     *
     * @return none
     */

    private void loadEmployees() {
        tabledata.setDefaultRenderer(Object.class, new Render());
        DefaultTableModel model = (DefaultTableModel) tabledata.getModel();
        model.setRowCount(0);
        List<EmployeeObject> list = employeeController.employeeObjectList();
        listAux = list;
        if (list != null) {
            for (EmployeeObject employeeObject : list) {
                model.addRow(new Object[]{employeeObject.getFirstName(), employeeObject.getOthersName(), employeeObject.getFirstSurname(),
                        employeeObject.getTypeIdentification(), employeeObject.getIdentification(), employeeObject.getCountry(),
                        employeeObject.getEmail(), employeeObject.getStateEmployee()});
            }
        }
        tabledata.setModel(model);
    }


    /**
     * Get and show all countries in view
     *
     * @return none
     */
    private void loadCountries() {
        group = new ButtonGroup();
        JRadioButton aux;
        List<CountryObject> list = countryController.countryObjectList();
        int size = list.size();

        countriesOption = new JRadioButton[size];
        for (int i = 0; i < size; i++) {
            aux = new JRadioButton();
            aux.setText(list.get(i).getNameCountry());
            countriesOption[i] = aux;
            group.add(countriesOption[i]);
            panelCountry.add(countriesOption[i]);
        }
    }

    /**
     * Get and show all typeIdentification in view
     *
     * @return none
     */
    private void loadTypeIdentification() {
        for (TypeIdentificationObject typeIdentificationObject : typeIdentificationController.typeIdentificationModelList()) {
            typeIdentification.addItem(typeIdentificationObject.getNameIdentification());
        }
    }

    /**
     * Get and show all area in view
     *
     * @return none
     */

    private void loadArea() {
        for (AreaObject areaObject : areaController.areaObjectList()) {
            area.addItem(areaObject.getNameArea());
        }
    }

    /**
     * Insert employee
     *
     * @return none
     */
    private void insertEmployee() {
        String country = "", newMail = "";
        int response, idCountry;
        if (validation.formValidation(textFirtsSurname,
                textSecondSurname,
                textFirstName,
                textIdentification,
                countriesOption)) {
            if (countriesOption[0].isSelected()) {
                country = countriesOption[0].getText();
                idCountry = 1;
            } else {
                country = countriesOption[1].getText();
                idCountry = 2;
            }
            if (validation.dateValidation(admissionDate)) {
                newMail = utilities.generateMail(textFirstName.getText(), textFirtsSurname.getText(), country);
                response = employeeController.validateEmployeeEmail(newMail);
                if (response == 0) {

                    if (employeeController.insertEmployee(new EmployeeObject(textFirtsSurname.getText(),
                            textSecondSurname.getText(), textFirstName.getText(), textOtherName.getText(),
                            textIdentification.getText(), admissionDate.getDatoFecha(),
                            newMail, (typeIdentification.getSelectedIndex() + 1), (area.getSelectedIndex() + 1), idCountry)) > 0) {
                        JOptionPane.showMessageDialog(panel1, "Datos Registrados");
                        cleanForm();
                    } else {
                        JOptionPane.showMessageDialog(panel1, "ERROR... Datos no Registrados");
                    }
                } else {
                    newMail = utilities.generateMail(textFirstName.getText(), (textFirtsSurname.getText() +
                            utilities.ReadModifyTxt()), country);
                    System.out.println(newMail);
                    response = employeeController.validateEmployeeEmail(newMail);
                    if (response == 0) {
                        if (employeeController.insertEmployee(new EmployeeObject(textFirtsSurname.getText(),
                                textSecondSurname.getText(), textFirstName.getText(), textOtherName.getText(),
                                textIdentification.getText(), admissionDate.getDatoFecha(),
                                newMail, (typeIdentification.getSelectedIndex() + 1), (area.getSelectedIndex() + 1), idCountry)) > 0) {
                            JOptionPane.showMessageDialog(panel1, "Datos Registrados");
                            cleanForm();

                        } else {
                            JOptionPane.showMessageDialog(panel1, "ERROR... Datos no Registrados");
                        }
                    }


                }
            } else {
                JOptionPane.showMessageDialog(panel1, "La fecha seleccionada no puede ser superior" +
                        " a la fecha actual");
                textFirstName.setRequestFocusEnabled(true);
            }
        } else {
            JOptionPane.showMessageDialog(panel1, "Datos Incompletos");
            textFirstName.setRequestFocusEnabled(true);
        }


    }

    /**
     * Update employee
     *
     * @return none
     */
    private void updateEmployee() {
        String country = "", newMail = "";
        int response, idCountry;
        if (validation.formValidation(textFirtsSurname,
                textSecondSurname,
                textFirstName,
                textIdentification,
                countriesOption)) {
            if (countriesOption[0].isSelected()) {
                country = countriesOption[0].getText();
                idCountry = 1;
            } else {
                country = countriesOption[1].getText();
                idCountry = 2;
            }
            if (validation.dateValidation(admissionDate)) {
                newMail = utilities.generateMail(textFirstName.getText(), textFirtsSurname.getText(), country);
                response = employeeController.validateEmployeeEmailUpdate(newMail, textIdentification.getText());
                if (response == 0) {

                    if (employeeController.updateEmployee(new EmployeeObject(textFirtsSurname.getText(),
                            textSecondSurname.getText(), textFirstName.getText(), textOtherName.getText(),
                            textIdentification.getText(), admissionDate.getDatoFecha(),
                            newMail, (typeIdentification.getSelectedIndex() + 1), (area.getSelectedIndex() + 1), idCountry), ident) > 0) {
                        JOptionPane.showMessageDialog(panel1, "Datos Modificados");
                        cleanForm();
                    } else {
                        JOptionPane.showMessageDialog(panel1, "ERROR... Datos no Modificados");
                    }
                } else {
                    newMail = utilities.generateMail(textFirstName.getText(), (textFirtsSurname.getText() +
                            utilities.ReadModifyTxt()), country);
                    response = employeeController.validateEmployeeEmailUpdate(newMail, textIdentification.getText());
                    if (response == 0) {
                        if (employeeController.updateEmployee(new EmployeeObject(textFirtsSurname.getText(),
                                textSecondSurname.getText(), textFirstName.getText(), textOtherName.getText(),
                                textIdentification.getText(), admissionDate.getDatoFecha(),
                                newMail, (typeIdentification.getSelectedIndex() + 1), (area.getSelectedIndex() + 1), idCountry), ident) > 0) {
                            JOptionPane.showMessageDialog(panel1, "Datos Modificados");
                            cleanForm();
                        } else {
                            JOptionPane.showMessageDialog(panel1, "ERROR... Datos no Modificados");
                        }
                    }


                }
            } else {
                JOptionPane.showMessageDialog(panel1, "La fecha seleccionada no puede ser superior" +
                        " a la fecha actual");
                textFirstName.setRequestFocusEnabled(true);
            }
        } else {
            JOptionPane.showMessageDialog(panel1, "Datos Incompletos");
            textFirstName.setRequestFocusEnabled(true);
        }
    }

    /**
     * send data to form
     *
     * @return none
     */
    private void sendDataForm(int selected) {
        String identification = (String) tabledata.getValueAt(selected, 4);
        EmployeeObject employeeObject = employeeController.employeeObjectID(identification);
        ident = employeeObject.getIdentification();
        if (employeeObject != null) {
            textFirtsSurname.setText(employeeObject.getFirstSurname());
            textSecondSurname.setText(employeeObject.getSecondSurname());
            textFirstName.setText(employeeObject.getFirstName());
            textIdentification.setText(employeeObject.getIdentification());
            if (employeeObject.getIdCountry() == 0) {
                countriesOption[0].setSelected(true);
            } else {
                countriesOption[1].setSelected(true);
            }

            area.setSelectedIndex(employeeObject.getIdArea());
            typeIdentification.setSelectedIndex(employeeObject.getIdTypeIdentification());
            admissionDate.setDatoFecha(employeeObject.getDateEntry());
            titleForm.setText("Actualizar Empleado");
            btnRegister.setText("Actualizar");
            card.show(panelCenter, "CardForm");
        }


    }

    /**
     * clean form
     *
     * @return none
     */
    private void cleanForm() {
        textFirtsSurname.setText("");
        textSecondSurname.setText("");
        textFirstName.setText("");
        textIdentification.setText("");
        textOtherName.setText("");
        textFirtsSurname.setText("");
        countriesOption[0].setSelected(false);
        countriesOption[1].setSelected(false);
        area.setSelectedIndex(0);
        typeIdentification.setSelectedIndex(0);
        admissionDate.setDatoFecha(new Date());

    }

    /**
     * Filter employee with filter
     *
     * @return none
     */
    private void filterDataEmployee(int filter, String value) {

        tabledata.setDefaultRenderer(Object.class, new Render());
        DefaultTableModel model = (DefaultTableModel) tabledata.getModel();
        model.setRowCount(0);

        if (value.trim().isEmpty()) {

            if (listAux != null) {
                for (EmployeeObject employeeObject : listAux) {
                    model.addRow(new Object[]{employeeObject.getFirstName(), employeeObject.getOthersName(), employeeObject.getFirstSurname(),
                            employeeObject.getTypeIdentification(), employeeObject.getIdentification(), employeeObject.getCountry(),
                            employeeObject.getEmail(), employeeObject.getStateEmployee()});
                }
            }
            tabledata.setModel(model);
        } else {
            if (listAux != null) {
                for (EmployeeObject employeeObject : listAux) {
                    switch (filter) {
                        case 0:
                            if (employeeObject.getFirstName().substring(0, value.length()).equals(value)) {
                                model.addRow(new Object[]{employeeObject.getFirstName(), employeeObject.getOthersName(), employeeObject.getFirstSurname(),
                                        employeeObject.getTypeIdentification(), employeeObject.getIdentification(), employeeObject.getCountry(),
                                        employeeObject.getEmail(), employeeObject.getStateEmployee()});
                            }
                            break;
                        case 1:
                            if (!employeeObject.getOthersName().trim().isEmpty()) {
                                if (employeeObject.getOthersName().substring(0, value.length()).equals(value)) {
                                    model.addRow(new Object[]{employeeObject.getFirstName(), employeeObject.getOthersName(), employeeObject.getFirstSurname(),
                                            employeeObject.getTypeIdentification(), employeeObject.getIdentification(), employeeObject.getCountry(),
                                            employeeObject.getEmail(), employeeObject.getStateEmployee()});
                                }
                            }
                            break;
                        case 2:
                            if (employeeObject.getFirstSurname().substring(0, value.length()).equals(value)) {
                                model.addRow(new Object[]{employeeObject.getFirstName(), employeeObject.getOthersName(), employeeObject.getFirstSurname(),
                                        employeeObject.getTypeIdentification(), employeeObject.getIdentification(), employeeObject.getCountry(),
                                        employeeObject.getEmail(), employeeObject.getStateEmployee()});
                            }
                            break;
                        case 3:
                            if (employeeObject.getTypeIdentification().substring(0, value.length()).equals(value)) {
                                model.addRow(new Object[]{employeeObject.getFirstName(), employeeObject.getOthersName(), employeeObject.getFirstSurname(),
                                        employeeObject.getTypeIdentification(), employeeObject.getIdentification(), employeeObject.getCountry(),
                                        employeeObject.getEmail(), employeeObject.getStateEmployee()});
                            }
                            break;
                        case 4:
                            if (employeeObject.getIdentification().substring(0, value.length()).equals(value)) {
                                model.addRow(new Object[]{employeeObject.getFirstName(), employeeObject.getOthersName(), employeeObject.getFirstSurname(),
                                        employeeObject.getTypeIdentification(), employeeObject.getIdentification(), employeeObject.getCountry(),
                                        employeeObject.getEmail(), employeeObject.getStateEmployee()});
                            }
                            break;
                        case 5:
                            if (employeeObject.getCountry().substring(0, value.length()).equals(value)) {
                                model.addRow(new Object[]{employeeObject.getFirstName(), employeeObject.getOthersName(), employeeObject.getFirstSurname(),
                                        employeeObject.getTypeIdentification(), employeeObject.getIdentification(), employeeObject.getCountry(),
                                        employeeObject.getEmail(), employeeObject.getStateEmployee()});
                            }
                            break;
                        case 6:
                            if (employeeObject.getEmail().substring(0, value.length()).equals(value)) {
                                model.addRow(new Object[]{employeeObject.getFirstName(), employeeObject.getOthersName(), employeeObject.getFirstSurname(),
                                        employeeObject.getTypeIdentification(), employeeObject.getIdentification(), employeeObject.getCountry(),
                                        employeeObject.getEmail(), employeeObject.getStateEmployee()});
                            }
                            break;
                        case 7:
                            if (employeeObject.getStateEmployee().substring(0, value.length()).equals(value)) {
                                model.addRow(new Object[]{employeeObject.getFirstName(), employeeObject.getOthersName(), employeeObject.getFirstSurname(),
                                        employeeObject.getTypeIdentification(), employeeObject.getIdentification(), employeeObject.getCountry(),
                                        employeeObject.getEmail(), employeeObject.getStateEmployee()});
                            }
                            break;


                    }

                }
            }
            tabledata.setModel(model);
        }

    }

    /**
     * Delete employee with identification
     *
     * @return none
     */

    private void deleteEmployee(int selected) {
        String identification = (String) tabledata.getValueAt(selected, 4);
        if (employeeController.deleteEmployee(identification) > 0) {
            JOptionPane.showMessageDialog(panel1, "Empleado Eliminado");
        }
    }

}
