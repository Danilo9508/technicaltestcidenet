package com.cidenet.demo.interfaces;


import com.cidenet.demo.modelObjects.TypeIdentificationObject;


import java.util.List;
/**
 * @author Danilo Zúñiga
 * @version 1.0.0
 * @date 01/03/2020
 */

public interface ITypeIdentificationModel {
    /**
     * Get the list of all the typeIdentification
     * @return the list of the typeIdentificationObject
     */
    public List<TypeIdentificationObject> typeIdentificationModelList();
}
