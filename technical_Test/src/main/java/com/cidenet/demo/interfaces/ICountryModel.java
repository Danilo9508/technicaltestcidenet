package com.cidenet.demo.interfaces;

import com.cidenet.demo.modelObjects.CountryObject;

import java.util.List;
/**

 * @author Danilo Zuñiga
 * @version 1.0.0
 * @date 03/03/2020
 */
public interface ICountryModel {
    /**
     * Get the list of all the countries
     * @return the list of the countries
     */
    public List<CountryObject> countryObjectList();
}
