package com.cidenet.demo.interfaces;

import com.cidenet.demo.modelObjects.AreaObject;
import com.cidenet.demo.modelObjects.CountryObject;

import java.util.List;

/**

 * @author Danilo Zúñiga
 * @version 1.0.0
 * @date 01/03/2020
 */
public interface IAreaModel {
    /**
     * Get the list of all the countries
     * @return the list of the countries
     */
    public List<AreaObject> areaObjectList();
}
