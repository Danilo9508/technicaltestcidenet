package com.cidenet.demo.interfaces;

import com.cidenet.demo.modelObjects.EmployeeObject;

import java.util.List;

/**
 * @author Danilo Zuñiga
 * @version 1.0.0
 * @date 03/03/2020
 */
public interface IEmployeeModel {
    /**
     * Validate if the employee's email exists
     *
     * @return Integer
     */

    public int validateEmployeeEmail(String mail);

    /**
     * Validate if the employee's email exists
     *
     * @return Integer
     */

    public int validateEmployeeEmailUpdate(String mail,String cc);
    /**
     * Get the list of all the employee
     *
     * @return the list of the employee
     */
    public List<EmployeeObject> employeeObjectList();

    /**
     * Insert employee data
     *
     * @return boolean
     */
    public int insertEmployee(EmployeeObject employeeObject);
    /**
     * Update employee data
     *
     * @return boolean
     */
    public int updateEmployee(EmployeeObject employeeObject, String cc);

    /**
     * Get the list of all the employee
     *
     * @return the list of the employee
     */

    public EmployeeObject employeeObjectID(String identification);

    /**
     * Delete employee
     *
     * @return 1 if delete or 0 if not delete
     */
    public int deleteEmployee(String ident);
}
