package com.cidenet.demo.complements;


import org.springframework.stereotype.Repository;
import rojeru_san.componentes.RSDateChooser;

import javax.swing.*;
import java.awt.event.KeyEvent;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Danilo Zuñiga
 * @version 1.0.0
 * @date 03/03/2020
 */
@Repository
public class Validation {

    /**
     * Validate the form data
     *
     * @return boolean
     */
    public boolean formValidation(JTextField firstSurname,
                                  JTextField secondSurname,
                                  JTextField name,
                                  JTextField identification,
                                  JRadioButton[] countries) {
        return !firstSurname.getText().trim().isEmpty() && !secondSurname.getText().trim().isEmpty()
                && !name.getText().trim().isEmpty() && !identification.getText().trim().isEmpty()
                && (countries[0].isSelected() || countries[1].isSelected());
    }

    /**
     * Validate the data of the date of entry
     *
     * @return boolean
     */

    public boolean dateValidation(RSDateChooser dateChooser) {
        return dateChooser.getDatoFecha().compareTo(new Date()) < 0;
    }

    /**
     * Validate that only letters and numbers with spaces are entered
     *
     * @return none
     */

    public void validateOnlyLetters(KeyEvent event) {
        String pattern = "[\\p{Alpha}\\p{Space}]";
        Pattern p = Pattern.compile(pattern);
        Matcher m = p.matcher(Character.toString(event.getKeyChar()));
        if (!m.find()) {
            event.consume();

        } else {

            if (Character.isLowerCase(event.getKeyChar())) {
                String msj = ("" + event.getKeyChar()).toUpperCase();
                event.setKeyChar(msj.charAt(0));
            }
        }

    }

    /**
     * Validate that only letters and numbers are entered without spaces
     *
     * @return none
     */

    public void validateOnlyNumberLetters(KeyEvent event) {
        String pattern = "[\\p{Alnum}]";
        Pattern p = Pattern.compile(pattern);
        Matcher m = p.matcher(Character.toString(event.getKeyChar()));
        if (!m.find()) {
            event.consume();

        }
    }
    /**
     * Validate buttons
     *
     * @return none
     */
    public void validateButtons(JButton edit, JButton delete, int selected){
        if(selected>=0){
            edit.setEnabled(true);
            delete.setEnabled(true);

        }
    }




}
