package com.cidenet.demo.complements;

import org.springframework.stereotype.Repository;

import java.io.*;
import java.util.Scanner;

/**
 * @author Danilo Zuñiga
 * @version 1.0.0
 * @date 03/03/2020
 */
@Repository
public class Utilities {

    /**
     * Dynamically generate emails for the employee
     *
     * @return none firstSurName
     */

    public String generateMail(String firstName, String firstSurName, String country) {
        String domain;
        if (country.equals("COLOMBIA")) {
            domain = "CIDENET.COM.CO";
        } else {
            domain = "CIDENET.COM.US";
        }
        return firstName.trim().replace(" ", "")
                + "." + firstSurName.trim().replace(" ", "")
                + "@" + domain;
    }

    /**
     * Read txt for the mail stream
     *
     * @return Integer
     */
    public int ReadModifyTxt(){

        int outputNumber=0;
        try {
            File file = new File("src/main/resources/codeMail.txt");

            if(file.exists()){
                Scanner scanner = new Scanner(file);
                if (scanner.hasNextLine()){
                    outputNumber =scanner.nextInt();
                    scanner.close();
                    wiriteFile(outputNumber);
                }
            }
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
        return outputNumber;
    }
    /**
     * Modify txt for the mail stream
     *
     * @return none
     */
    private void wiriteFile(int data){
        try {
            data++;

            File file = new File("src/main/resources/codeMail.txt");
            FileWriter fileWriter = new FileWriter(file);
            fileWriter.write(data+"");
            fileWriter.close();
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
    }
}
