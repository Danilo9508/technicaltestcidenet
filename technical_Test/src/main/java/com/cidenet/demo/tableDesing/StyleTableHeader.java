
package com.cidenet.demo.tableDesing;

import javax.swing.*;
import javax.swing.table.TableCellRenderer;
import java.awt.*;


public class StyleTableHeader implements TableCellRenderer {

    int R, G, B;
    //colores de la barra superior

    public StyleTableHeader() {
         R=0;
         G=102;
         B=0;
    }

    public StyleTableHeader(int R, int G, int B) {
        this.R = R;
        this.G = G;
        this.B = B;
    }

    @Override
    public Component getTableCellRendererComponent(JTable jtable, Object value, boolean bln, boolean bln1, int row, int column) {
        JComponent jcomponent = null;

        if (value instanceof String) {
            jcomponent = new JLabel((String) "   " + value);
            ((JLabel) jcomponent).setHorizontalAlignment(SwingConstants.CENTER);
            ((JLabel) jcomponent).setSize(30, jcomponent.getWidth());
            ((JLabel) jcomponent).setPreferredSize(new Dimension(3, jcomponent.getWidth()));
        }

        jcomponent.setEnabled(true);
        jcomponent.setBorder(javax.swing.BorderFactory.createMatteBorder(1, 1, 0, 1, new Color(R, G, B)));
        jcomponent.setOpaque(true);
        //jcomponent.setBackground( new Color(58, 159, 171) );
        
        jcomponent.setBackground(new Color(R, G, B));
        jcomponent.setForeground(Color.WHITE);
        jcomponent.setFont(new Font("Fira Code", Font.BOLD, 15));

        return jcomponent;
    }

}
