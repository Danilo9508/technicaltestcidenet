package com.cidenet.demo.tableDesing;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.*;

/**
 *
 * @author DANILO
 */
public class StyleTableRenderer extends DefaultTableCellRenderer {

    private Component componenete;
    int R, G, B, RL, GL, BL;
    // colores de seleccion y lineas internas 
    public StyleTableRenderer() {
        R = 0;
        G = 153;
        B = 0;
    }

    public StyleTableRenderer(int R, int G, int B, int RL, int GL, int BL) {
        this.R = R;
        this.G = G;
        this.B = B;
        this.RL = RL;
        this.GL = GL;
        this.BL = BL;
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        table.setShowVerticalLines(false); 
        componenete = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column); //To change body of generated methods, choose Tools | Templates.       
        
        this.setHorizontalAlignment(0);
        this.setBorder(null);
        //color de las lineas
        this.setBorder(javax.swing.BorderFactory.createMatteBorder(0, 1, 1, 1, new Color(RL, GL, BL)));

        if (row % 2 == 0) {
            componenete.setForeground(new Color(0, 0, 0));
            componenete.setBackground(new Color(255, 255, 255));
        } else {
            componenete.setForeground(new Color(0, 0, 0));
            componenete.setBackground(new Color(255, 255, 255));
        }
        if (isSelected) {
            componenete.setForeground(Color.white);
            //color del seleccion
            componenete.setBackground(new Color(R, G, B));
            componenete.setFont(new Font("Fira Code", Font.BOLD, 14));

        }

        return componenete;

    }

}
